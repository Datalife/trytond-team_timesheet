# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool

from .configuration import Configuration, ConfigurationSequence
from .employee_team import Employee, Team
from .team_timesheet import (
    CreateWorkTimesheets, CreateWorkTimesheetStart, EditEmployeeTimesheets,
    EditEmployeeTimesheetsStart, TeamTimesheet, TeamTimesheetEmployee,
    TeamTimesheetWork)
from .timesheet import Timesheet, Work, WorkTeam


def register():
    Pool.register(
        Configuration,
        ConfigurationSequence,
        CreateWorkTimesheetStart,
        EditEmployeeTimesheetsStart,
        Employee,
        TeamTimesheet,
        TeamTimesheetEmployee,
        TeamTimesheetWork,
        WorkTeam,
        Work,
        Team,
        Timesheet,
        module='team_timesheet', type_='model')
    Pool.register(
        CreateWorkTimesheets,
        EditEmployeeTimesheets,
        module='team_timesheet', type_='wizard')
