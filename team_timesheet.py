# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields, Workflow
from trytond.model import Unique
from trytond.pool import Pool
from trytond.pyson import Eval, If, Bool, Not, Equal
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.exceptions import UserError, UserWarning
from trytond.i18n import gettext
from datetime import timedelta
from trytond.transaction import Transaction
from trytond.modules.timesheet_absolute_hours import get_absolute_hours_time
from trytond.modules.company_employee_team.employee_team \
    import EmployeeOrderMixin
from sql.functions import CharLength
from itertools import groupby

__all__ = ['TeamTimesheet', 'TeamTimesheetWork', 'TeamTimesheetEmployee',
           'EditEmployeeTimesheetsStart', 'EditEmployeeTimesheets',
           'CreateWorkTimesheetStart', 'CreateWorkTimesheets']


class TeamTimesheet(Workflow, ModelView, ModelSQL):
    """Team Timesheet"""
    __name__ = 'timesheet.team.timesheet'
    _rec_name = 'code'

    code = fields.Char('Code', required=True, select=True,
        states={'readonly': Eval('code_readonly', True)},
        depends=['code_readonly'])
    code_readonly = fields.Function(
        fields.Boolean('Code Readonly'),
        'get_code_readonly')
    date = fields.Date('Date', required=True, select=True,
        states={
            'readonly': ~Eval('state').in_(['draft', 'waiting'])},
        depends=['state'])
    team = fields.Many2One('company.employee.team', 'Team', select=True,
        domain=[('company', '=', Eval('company'))],
        states={
            'readonly':
                Not(Equal(Eval('state'), 'draft'))
                | Bool(Eval('works'))
                | Bool(Eval('employees')),
            'required': Bool(~Eval('num_workers'))},
        context={'date': Eval('date')},
        depends=['company', 'works', 'state', 'employees', 'date',
            'num_workers'])
    description = fields.Text('Description', depends=['date'])
    timesheets = fields.Function(
        fields.Many2Many('timesheet.line', None, None, 'Timesheets'),
        'get_timesheets', searcher='search_timesheets')
    works = fields.One2Many(
        'timesheet.team.timesheet-timesheet.work', 'team_timesheet', 'Works',
        depends=['allowed_employees', 'state'],
        context={
            'allowed_employees': Eval('allowed_employees'),
            'absolute_hours_time': get_absolute_hours_time()},
        states={
            'readonly': ~Eval('state').in_(['draft', 'waiting']),
            'required': Eval('state').in_(['confirmed', 'done'])})
    allowed_works = fields.Function(
        fields.Many2Many(
            'timesheet.work', None, None, 'Allowed works',
            readonly=True, required=False),
        'on_change_with_allowed_works')
    employees = fields.One2Many(
        'timesheet.team.timesheet-company.employee', 'team_timesheet',
        'Employees',
        states={
            'readonly': ~Eval('state').in_(['draft', 'waiting']),
            'required': (Eval('state').in_(['confirmed', 'done']) &
                Bool(~Eval('num_workers'))),
            'invisible': Bool(Eval('num_workers'))},
        context={
            'absolute_hours_time': get_absolute_hours_time()},
        order=[('employee', 'ASC')],
        depends=['state', 'num_workers'])
    num_workers = fields.Integer("Number of workers",
        domain=['OR',
            ('num_workers', '>', 0),
            ('num_workers', '=', None)],
        states={
            'required': Bool(~Eval('employees')),
            'invisible': Bool(Eval('employees'))
        },
        depends=['team', 'employees'])
    allowed_employees = fields.Function(
        fields.Many2Many('company.employee', None, None, 'Allowed employees',
            readonly=True, required=False),
        'on_change_with_allowed_employees')
    company = fields.Many2One(
        'company.company', 'Company',
        select=True, required=True,
        states={'readonly':
            Not(Equal(Eval('state'), 'draft')) | Bool(Eval('team')) |
            Bool(Eval('works')) | Bool(Eval('have_employees'))},
        depends=['state', 'works', 'have_employees', 'team'])
    state = fields.Selection([
        ('draft', 'Draft'),
        ('waiting', 'Waiting'),
        ('confirmed', 'Confirmed'),
        ('done', 'Done'),
        ('cancelled', 'Cancelled')], 'State',
        readonly=True, required=True)
    have_employees = fields.Function(
        fields.Boolean('Have employees'),
        'get_have_employees')

    @classmethod
    def __setup__(cls):
        super(TeamTimesheet, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('code_uk1', Unique(t, t.code),
                'team_timesheet.msg_timesheet_team_timesheet_code_uk1')]
        cls._order = [
            ('date', 'DESC'),
            ('id', 'DESC'),
            ]
        cls._transitions |= set((
                ('draft', 'waiting'),
                ('waiting', 'confirmed'),
                ('confirmed', 'done'),
                ('waiting', 'draft'),
                ('draft', 'cancelled'),
                ('cancelled', 'draft'),
                ('done', 'draft')
                ))
        cls._buttons.update({
            'cancel': {
                'invisible': Eval('state') != 'draft',
                'depends': ['state']
            },
            'draft': {
                'invisible': ~Eval('state').in_(['cancelled', 'waiting', 'done']),
                'icon': If(Eval('state') == 'cancelled', 'tryton-undo',
                    'tryton-back'),
                'depends': ['state']
            },
            'wait': {
                'invisible': Eval('state') != 'draft',
                'icon': If(Eval('state') == 'draft', 'tryton-forward',
                    'tryton-back'),
                'depends': ['state']
            },
            'confirm': {
                'invisible': Eval('state') != 'waiting',
                'depends': ['state'],
            },
            'do': {
                'invisible': Eval('state') != 'confirmed',
                'depends': ['state']
            }
        })
        cls._order.insert(0, ('date', 'ASC'))
        cls._order.insert(1, ('team', 'ASC'))

    @classmethod
    def __register__(cls, module_name):
        table = cls.__table_handler__(module_name)
        sql_table = cls.__table__()
        cursor = Transaction().connection.cursor()

        super().__register__(module_name)

        table.not_null_action('team', action='remove')

        # Migration from 5.6: rename state cancel to cancelled
        cursor.execute(*sql_table.update(
                [sql_table.state], ['cancelled'],
                where=sql_table.state == 'cancel'))

    @staticmethod
    def order_code(tables):
        table, _ = tables[None]
        return [CharLength(table.code), table.code]

    @staticmethod
    def default_code_readonly():
        model_config = Pool().get('timesheet.configuration')
        config = model_config(1)
        return bool(config.team_timesheet_sequence)

    @classmethod
    def get_timesheets(cls, records, name):
        return {r.id: [t.id for w in r.works for t in w.timesheets]
            for r in records}

    def get_code_readonly(self, name):
        return True

    @classmethod
    def create(cls, vlist):
        Config = Pool().get('timesheet.configuration')

        config = Config(1)
        default_company = cls.default_company()
        vlist = [x.copy() for x in vlist]
        for values in vlist:
            if not values.get('code'):
                values['code'] = config.get_multivalue(
                    'team_timesheet_sequence',
                    company=values.get('company', default_company)).get()
        return super(TeamTimesheet, cls).create(vlist)

    @classmethod
    def copy(cls, team_timesheet, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['code'] = None
        default['timesheets'] = None
        return super(TeamTimesheet, cls).copy(team_timesheet, default=default)

    @classmethod
    def delete(cls, timesheets):
        # Cancel before delete
        cls.cancel(timesheets)
        for sheet in timesheets:
            if sheet.state != 'cancelled':
                raise UserError(gettext(
                    'team_timesheet.msg_timesheet_team_timesheet_delete_cancel',
                    sheet=sheet.rec_name))
        super(TeamTimesheet, cls).delete(timesheets)

    @staticmethod
    def default_date():
        Date_ = Pool().get('ir.date')
        return Date_.today()

    @classmethod
    def default_state(cls):
        return 'draft'

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')

    @staticmethod
    def default_team():
        return Transaction().context.get('team', None)

    @classmethod
    def search_timesheets(cls, name, clause):
        text = 'works.{}'.format(name)
        return [(text,) + tuple(clause[1:])]

    @fields.depends('employees', 'team', 'date', 'state')
    def _extend_employees(self):
        TTsEmployee = Pool().get('timesheet.team.timesheet-company.employee')
        employees = {tse.employee.id: tse for tse in self.employees}
        new_employees = []
        if self.team:
            with Transaction().set_context(date=self.date):
                for employee_id in self.team.get_employees():
                    if employee_id in employees:
                        new_employees.append(employees[employee_id])
                    else:
                        new_employees.append(
                            TTsEmployee(
                                employee=employee_id,
                                tts_state=self.state))
        self.employees = new_employees

    @fields.depends(methods=['_extend_employees'])
    def on_change_team(self):
        self._extend_employees()

        self.num_workers = None

    @fields.depends('works', methods=['_extend_employees'])
    def on_change_date(self):
        self._extend_employees()
        for work in self.works:
            work._update_date(self.date)

    @fields.depends('works')
    def on_change_with_allowed_works(self, name='allowed_works'):
        if self.works:
            return list(set([w.work.id for w in self.works if w.work]))
        return []

    @fields.depends('employees')
    def on_change_with_allowed_employees(self, name='allowed_employees'):
        if self.employees:
            employees = [e.employee for e in self.employees if e.valid()]
            return list(set(map(int, employees)))
        return []

    @classmethod
    def get_have_employees(cls, records, name=None):
        pool = Pool()
        TTsEmployee = pool.get('timesheet.team.timesheet-company.employee')
        tts_employee = TTsEmployee.__table__()
        cursor = Transaction().connection.cursor()

        ts_ids = list(map(int, records))
        query = tts_employee.select(
            tts_employee.team_timesheet,
            where=(tts_employee.team_timesheet.in_(ts_ids)),
            group_by=tts_employee.team_timesheet
        )
        cursor.execute(*query)
        query_result = cursor.fetchall()
        result = {qr[0]: True for qr in query_result}
        for r in ts_ids:
            result.setdefault(r, False)
        return result

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, timesheets):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancel(cls, timesheets):
        pool = Pool()
        ModelData = pool.get('ir.model.data')
        User = pool.get('res.user')
        Group = pool.get('res.group')
        Line = pool.get('timesheet.line')

        def in_group():
            group = Group(ModelData.get_id('timesheet',
                'group_timesheet_admin'))
            transaction = Transaction()
            user_id = transaction.user
            if user_id == 0:
                user_id = transaction.context.get('user', user_id)
            if user_id == 0:
                return True
            user = User(user_id)
            return group in user.groups

        done_timesheets = [r for r in timesheets if r.state == 'done']

        if done_timesheets and not in_group():
            raise UserError(gettext(
                'team_timesheet.msg_timesheet_team_timesheet_cancel_done'))

        lines = [line for timesheet in timesheets
            for line in timesheet.timesheets]
        Line.delete(lines)

    @classmethod
    @ModelView.button
    @Workflow.transition('waiting')
    def wait(cls, timesheets):
        cls.create_timesheets(timesheets, 'all', skip_works=True)

    @classmethod
    @ModelView.button
    @Workflow.transition('confirmed')
    def confirm(cls, timesheets):
        for timesheet in timesheets:
            cls.check_employees_included(timesheet)

    @classmethod
    @ModelView.button
    @Workflow.transition('done')
    def do(cls, timesheets):
        pool = Pool()
        TimesheetLine = pool.get('timesheet.line')

        to_delete = []
        cls.check_unique_ts(timesheets)
        for timesheet in timesheets:
            for t in timesheet.timesheets:
                if t.date != timesheet.date:
                    raise UserError(gettext('team_timesheet.'
                        'msg_timesheet_team_timesheet_dates_must_match',
                        date=t.date,
                        sheet_date=timesheet.date))
                if t.total_hours == 0:
                    to_delete.append(t)
            cls.check_employees_included(timesheet)

        if to_delete:
            TimesheetLine.delete(to_delete)

    @classmethod
    def check_unique_ts(cls, timesheets):
        pool = Pool()
        Lang = pool.get('ir.lang')
        Warning = pool.get('res.user.warning')

        my_dictionary = {}
        for timesheet in timesheets:
            if not timesheet.team:
                continue
            if my_dictionary.get((timesheet.date, timesheet.team), None):
                duplicated = True
            else:
                my_dictionary[(timesheet.date, timesheet.team)] = timesheet.id
                duplicated = cls.search([
                    ('team', '=', timesheet.team),
                    ('date', '=', timesheet.date),
                    ('id', '!=', timesheet.id),
                    ('state', '=', 'done')])
            if duplicated:
                language = Transaction().language
                languages = Lang.search([('code', '=', language)])
                if not languages:
                    languages = Lang.search([('code', '=', 'en_US')])
                language, = languages
                formatted = language.strftime(timesheet.date)
                warning_name = 'duplicated_timesheet_%s_%s' % (timesheet.team,
                    formatted)
                if Warning.check(warning_name):
                    raise UserWarning(warning_name, gettext(
                        'team_timesheet.msg_timesheet_team_timesheet_duplicated_timesheet',
                        team=timesheet.team.rec_name,
                        date=formatted))

    @classmethod
    def check_employees_included(cls, timesheet):
        employees = set([line.employee for line in timesheet.timesheets])
        employees_tts = set([e.employee for e in timesheet.employees])
        if not employees.issubset(employees_tts):
            raise UserError(gettext(
                'team_timesheet.msg_timesheet_team_timesheet_employees_not_included',
                sheet=timesheet.rec_name))

    @classmethod
    def create_timesheets(cls, team_timesheets, creation_type,
            timesheets=None, employees=None, skip_works=False,
            tts_works=None):
        pool = Pool()
        lines = []
        for team_timesheet in team_timesheets:
            if skip_works:
                works = [w for w in team_timesheet.works if not w.timesheets]
            else:
                works = team_timesheet.works
            if tts_works:
                works = [w for w in works if w in tts_works]
            for work in works:
                lines.extend(work.create_timesheets(creation_type,
                    employees=employees))

        if lines:
            Lines = pool.get('timesheet.line')
            Lines.save(lines)

    def check_timesheet_changes(self, line):
        if self.state in {'confirmed', 'done'}:
            raise UserError(gettext(
                'team_timesheet.msg_timesheet_line_modify_line_tts_confirmed_done',
                line=line.rec_name,
                sheet=self.rec_name))
        return True

    @classmethod
    def write(cls, *args):
        pool = Pool()
        TimesheetLine = pool.get('timesheet.line')

        actions = iter(args)
        to_write = []
        for records, values in zip(actions, actions):
            if values.get('date'):
                timesheets = [t for r in records for t in r.timesheets]
                to_write.extend((timesheets, {'date': values['date']}))

        if to_write:
            TimesheetLine.write(*to_write)

        super().write(*args)


class TeamTimesheetDetail(object):
    team_timesheet = fields.Many2One('timesheet.team.timesheet',
        'Team Timesheet', ondelete='CASCADE', required=True, select=True,
        states={
            'readonly': (
                (Eval('tts_state') != 'draft')
                & Bool(Eval('team_timesheet'))),
        },
        depends=['tts_state'])
    tts_state = fields.Function(fields.Selection('_get_tts_states',
        'Team timesheet State'), 'on_change_with_tts_state')

    @classmethod
    def delete(cls, records):
        pool = Pool()
        Warning = pool.get('res.user.warning')

        tss = [ts for r in records
               for ts in r.timesheets if r.timesheets]
        if tss and cls._must_warn(records):
            warning_name = '%s.cannot_delete' % 0
            if Warning.check(warning_name):
                raise UserWarning(warning_name, gettext(
                    'team_timesheet.msg_timesheet_team_timesheet-timesheet_work_cannot_delete'))
            Timesheet = Pool().get('timesheet.line')
            Timesheet.delete(tss)

        super(TeamTimesheetDetail, cls).delete(records)

    @classmethod
    def _must_warn(cls, records):
        return True

    @classmethod
    def _get_tts_states(cls):
        return Pool().get('timesheet.team.timesheet').state.selection

    @fields.depends('team_timesheet', '_parent_team_timesheet.state')
    def on_change_with_tts_state(self, name=None):
        if self.team_timesheet:
            return self.team_timesheet.state


class TeamTimesheetWork(TeamTimesheetDetail, ModelView, ModelSQL):
    """TeamTimesheet - Work"""
    __name__ = 'timesheet.team.timesheet-timesheet.work'
    _table = 'timesheet_team_work_rel'

    team_works = fields.Function(
        fields.Many2Many('timesheet.work', None, None, 'Team works'),
        'get_team_works')
    work = fields.Many2One('timesheet.work', 'Work',
        ondelete='CASCADE', required=True, select=True,
        domain=[
            ('company', '=', Eval('_parent_team_timesheet',
                {}).get('company', None)),
            If(Bool(Eval('team_works', [])),
                ('id', 'in', Eval('team_works', [])),
                ())
        ],
        states={
            'readonly': (
                    Bool(Eval('timesheets'))
                    | (Eval('tts_state') != 'draft'))
        },
        depends=['timesheets', 'team_works', 'tts_state'])
    time_type = fields.Selection(
        [('team', 'Team time'),
         ('employee', 'Employee time'), ],
        'Time type', required=True,
        states={'readonly': (Eval('tts_state') != 'draft')},
        depends=['tts_state'])
    hours = fields.TimeDelta('Duration', converter='absolute_hours_time',
        required=True, states={
            'readonly': (Eval('tts_state') != 'draft')
        }, depends=['tts_state'])
    total_hours = fields.Function(
        fields.TimeDelta('Total hours', converter='absolute_hours_time'),
        'on_change_with_total_hours')
    planned_hours = fields.Function(
        fields.TimeDelta('Planned hours', converter='absolute_hours_time'),
        'on_change_with_planned_hours')
    timesheets = fields.One2Many('timesheet.line', 'tts_work',
        'Timesheet lines',
        order=[('employee', 'ASC')],
        domain=[('work', '=', Eval('work'))],
        states={'readonly': (Eval('tts_state') != 'draft')},
        depends=['work', 'tts_state'])

    @classmethod
    def __setup__(cls):
        super(TeamTimesheetWork, cls).__setup__()
        cls._buttons.update({
            'create_timesheets_wizard': {
                'readonly': (Eval('_parent_team_timesheet', {}).
                        get('state').in_(['confirmed', 'done', 'cancelled'])),
                'invisible': Bool(Eval('_parent_team_timesheet.num_workers'))
            }
        })

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)

        # Migration from 3.6: reduce table name length
        old_table = 'timesheet_team_timesheet-timesheet_work'
        if table_h.table_exist(old_table):
            table_h.table_rename(old_table, cls._table)

        super(TeamTimesheetWork, cls).__register__(module_name)

        # Drop TeamTimesheet - Work UK
        table_h.drop_constraint('work_uk1', table=old_table)
        table_h.drop_constraint('work_uk1')

    @classmethod
    def copy(cls, works, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['timesheets'] = None
        return super(TeamTimesheetWork, cls).copy(works, default=default)

    @staticmethod
    def default_time_type():
        return 'employee'

    @fields.depends('team_timesheet', '_parent_team_timesheet.team',
        'team_works')
    def on_change_team_timesheet(self):
        if self.team_timesheet and self.team_timesheet.team:
            self.work = self.team_timesheet.team.get_def_ts_available_work()

    @fields.depends('team_timesheet', '_parent_team_timesheet.team')
    def on_change_with_team_works(self):
        if getattr(self.team_timesheet, 'team', None):
            return self.get_team_works(None)
        return []

    @fields.depends('timesheets')
    def on_change_with_total_hours(self, name=None):
        total_hours = sum(ts.duration.total_seconds()
            for ts in self.timesheets if ts.duration)
        if not total_hours:
            return timedelta(hours=0)
        else:
            return timedelta(seconds=total_hours)

    @fields.depends('time_type', 'hours', 'team_timesheet',
        '_parent_team_timesheet.employees',
        '_parent_team_timesheet.num_workers')
    def on_change_with_planned_hours(self, name=None):
        if self.hours and self.time_type:
            if self.time_type == 'team':
                return self.hours
            if (self.time_type == 'employee' and self.team_timesheet and
                    self.team_timesheet.employees):
                return self.hours * len(self.team_timesheet.employees)
            if (self.time_type == 'employee' and
                    self.team_timesheet.num_workers):
                return self.hours * self.team_timesheet.num_workers
        return timedelta(hours=0)

    # TODO: searcher method
    def get_rec_name(self, name):
        return self.team_timesheet.rec_name + ' - ' + self.work.rec_name

    def _create_timesheets(self, creation_type, timesheets, hours,
            employees=[]):
        timesheets = list(timesheets) or []
        employees_in_work = list(set(ts.employee for ts in timesheets))
        if not employees:
            employees = [e.employee for e in self.team_timesheet.employees]
        for t in timesheets:
            if creation_type == 'all_employees':
                t.duration = hours

        Timesheet = Pool().get('timesheet.line')
        for tts_employee in self.team_timesheet.employees:
            if tts_employee.employee not in employees:
                continue
            if tts_employee.employee in employees_in_work:
                continue
            ts = self._get_timesheet(Timesheet, tts_employee, hours)
            if ts:
                timesheets.append(ts)
        return timesheets

    def _get_timesheet(self, Timesheet, tts_employee, hours):
        return Timesheet(
            company=tts_employee.employee.company,
            work=self.work,
            employee=tts_employee.employee,
            duration=hours,
            tts_work=self,
            date=self.team_timesheet.date
        )

    def _create_timesheets_employee(self, creation_type, timesheets,
            employees=None):
        return self._create_timesheets(creation_type, timesheets, self.hours,
            employees=employees)

    def _create_timesheets_team(self, creation_type, timesheets,
            employees=None):
        if not employees:
            employees = [e.employee for e in self.team_timesheet.employees]
        hours = timedelta(seconds=self.hours.total_seconds() / len(employees))
        return self._create_timesheets(creation_type, timesheets, hours,
            employees=employees)

    def create_timesheets(self, creation_type, timesheets=None,
            employees=None):
        timesheets = self.timesheets if not timesheets else timesheets

        if self.time_type == 'employee':
            return self._create_timesheets_employee(creation_type, timesheets,
                employees=employees)
        elif self.time_type == 'team':
            return self._create_timesheets_team(creation_type, timesheets,
                employees=employees)

    @classmethod
    @ModelView.button_action('team_timesheet.wizard_create_work_timesheets')
    def create_timesheets_wizard(cls, works):
        pass

    def _get_unique_key(self):
        return (self.work.id, )

    @classmethod
    def validate(cls, records):
        super(TeamTimesheetWork, cls).validate(records)

        # Ensure teamtimesheet works unique criteria
        cls._check_unique_key(records)

    @classmethod
    def _check_unique_key(cls, records):
        pool = Pool()
        Ttsw = pool.get('timesheet.team.timesheet-timesheet.work')

        records.sort(key=lambda x: x.team_timesheet)
        for tts, tts_works in groupby(records,
                key=lambda x: x.team_timesheet):
            tts_worksl = list(tts_works)
            tts_works_db = Ttsw.search([
                ('team_timesheet', '=', tts),
                ('id', 'not in', list(map(int, tts_worksl)))])
            tts_worksl.extend(tts_works_db)
            tts_worksl.sort(key=lambda x: x._get_unique_key())
            for unq_key, uk_ttsws in groupby(tts_worksl,
                    key=lambda x: x._get_unique_key()):
                uk_ttswsl = list(uk_ttsws)
                if len(uk_ttswsl) > 1:
                    raise UserError(gettext(
                        'team_timesheet.msg_timesheet_team_timesheet-timesheet_work_unique_key',
                        work=uk_ttswsl[0].work.rec_name,
                        sheet=uk_ttswsl[0].team_timesheet.rec_name))

    def get_team_works(self, name):
        return (self.team_timesheet.team and self.team_timesheet.team.works and
            list(map(int, self.team_timesheet.team.works)) or [])

    def _update_date(self, date):
        for line in self.timesheets:
            line.date = date


class TeamTimesheetEmployee(EmployeeOrderMixin, ModelView, ModelSQL,
        TeamTimesheetDetail):
    """TeamTimesheet - Employee"""
    __name__ = 'timesheet.team.timesheet-company.employee'
    _table = 'timesheet_team_employee_rel'

    employee = fields.Many2One('company.employee', 'Employee',
        ondelete='CASCADE', required=True, select=True,
        domain=[
            ('company', '=', Eval('_parent_team_timesheet',
                {}).get('company', None)),
            ['OR',
                ('start_date', '=', None),
                ('start_date', '<=', Eval('_parent_team_timesheet',
                    {}).get('date'))],
            ['OR',
                ('end_date', '=', None),
                ('end_date', '>=', Eval('_parent_team_timesheet',
                    {}).get('date'))]
                ],
        states={
            'readonly': (
                    Bool(Eval('timesheets'))
                    | (Eval('tts_state') != 'draft'))
        },
        depends=['team_timesheet', 'timesheets', 'tts_state'])
    hours = fields.Function(
        fields.TimeDelta('Hours', 'absolute_hours_time'),
        'on_change_with_hours')
    day_hours = fields.Function(
        fields.TimeDelta('Day hours', 'absolute_hours_time'),
        'on_change_with_day_hours')
    timesheets = fields.Function(
        fields.Many2Many('timesheet.line', None, None, 'Timesheets',
            readonly=False, required=False),
        'get_timesheets')

    @classmethod
    def __setup__(cls):
        super(TeamTimesheetEmployee, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('employee_uk1', Unique(t, t.team_timesheet, t.employee),
                'team_timesheet.msg_timesheet_team_timesheet-company_employee_employee_uk1')]

        cls._buttons.update({
            'edit_timesheets_wizard': {
                'readonly': Eval('_parent_team_timesheet', {}).
                            get('state').in_(['confirmed', 'done', 'cancelled']),
            }
        })

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)

        # Migration from 3.6: reduce table name length
        old_table = 'timesheet_team_timesheet-company_employee'
        if table_h.table_exist(old_table):
            table_h.table_rename(old_table, cls._table)

        super(TeamTimesheetEmployee, cls).__register__(module_name)

    def get_timesheets(self, name=None):
        if self.team_timesheet and self.employee:
            if self.team_timesheet.timesheets:
                return [ts.id for ts in self.team_timesheet.timesheets
                    if ts.employee and ts.employee == self.employee]
        return None

    @fields.depends('team_timesheet', '_parent_team_timesheet.timesheets',
        'employee')
    def on_change_with_hours(self, name=None):
        if self.team_timesheet and self.employee:
            if self.team_timesheet.timesheets:
                result = sum(ts.duration.total_seconds()
                    for ts in self.team_timesheet.timesheets
                    if ts.duration and ts.employee == self.employee)
                return timedelta(seconds=result)
        return timedelta(hours=0)

    @fields.depends('team_timesheet', '_parent_team_timesheet.date', 'hours',
        'employee')
    def on_change_with_day_hours(self, name=None):
        if self.employee:
            return (
                self.employee.compute_day_hours(self.team_timesheet.date,
                    team_timesheet=self.team_timesheet) + (
                    self.hours or timedelta(hours=0))
            )
        return timedelta(hours=0)

    @classmethod
    @ModelView.button_action('team_timesheet.wizard_edit_employee_timesheets')
    def edit_timesheets_wizard(cls, employees):
        pass

    @classmethod
    def delete(cls, employees):
        pool = Pool()
        Line = pool.get('timesheet.line')
        timesheets = []
        for employee in employees:
            timesheets.extend(employee.timesheets)
        Line.delete(timesheets)
        super(TeamTimesheetEmployee, cls).delete(employees)

    def valid(self):
        return bool(self.employee)


class EditTimesheetsStart(ModelView):
    team_timesheet = fields.Many2One('timesheet.team.timesheet',
        'Team Timesheet', readonly=True)
    date = fields.Date('Date', readonly=True)
    team = fields.Many2One('company.employee.team', 'Team', readonly=True)
    timesheets = fields.One2Many('timesheet.line', None, 'Timesheets',
        domain=[('date', '=', Eval('date'))],
        depends=['date'])


class EditTimesheets(Wizard):

    edit_timesheets = StateTransition()

    def transition_edit_timesheets(self):
        for ts in self.start.timesheets:
            ts.save()
        self._delete()
        return 'end'

    def _delete(self):
        DetModel = Pool().get(Transaction().context['active_model'])
        record = DetModel(Transaction().context['active_id'])
        Timesheet = Pool().get('timesheet.line')

        Timesheet.delete([ts for ts in record.timesheets if
            ts not in self.start.timesheets])

    def default_start(self, fields):
        DetModel = Pool().get(Transaction().context['active_model'])
        instance = DetModel(Transaction().context['active_id'])

        record = dict(
            team_timesheet=instance.team_timesheet.id,
            date=instance.team_timesheet.date,
            team=instance.team_timesheet.team.id
        )
        return record


class CreateWorkTimesheetStart(EditTimesheetsStart):
    """Edit team's timesheet of a work"""
    __name__ = (
        'timesheet.team.timesheet-timesheet.work.create_timesheets.start')

    work = fields.Many2One('timesheet.work', 'Work', readonly=True)
    hours = fields.TimeDelta('Hours', 'company_work_time')
    planned_hours = fields.TimeDelta('Planed hours', 'company_work_time')
    allowed_employees = fields.One2Many('company.employee', None,
        'Allowed employees', readonly=True, required=False)
    time_type = fields.Selection(
        [('team', 'Team time'),
         ('employee', 'Employee time'), ],
        'Time type', required=False, readonly=True)
    creation_type = fields.Selection(
        [('all_employees', 'All employees'),
         ('new_employees_only', 'New employees only'),
         ('save_only', 'Only save manual changes'), ],
        'Creation type', required=False, readonly=False)

    @classmethod
    def __setup__(cls):
        super(CreateWorkTimesheetStart, cls).__setup__()
        cls.timesheets.domain.extend([
            ('work', '=', Eval('work')),
            ('employee', 'in', Eval('allowed_employees'))])
        cls.timesheets.depends.extend(['allowed_employees', 'work'])


class CreateWorkTimesheets(EditTimesheets):
    """Edit team's timesheet of a work"""
    __name__ = 'timesheet.team.timesheet-timesheet.work.create_timesheets'

    start = StateView(
        'timesheet.team.timesheet-timesheet.work.create_timesheets.start',
        'team_timesheet.create_work_timesheets_start_view_form',
        [Button('Cancel', 'end', 'tryton-cancel'),
         Button('Update', 'edit_timesheets', 'tryton-ok', default=True)])

    def transition_edit_timesheets(self):
        if self.start.creation_type != 'save_only':
            TTSWork = Pool().get(Transaction().context['active_model'])
            tts_work = TTSWork(Transaction().context['active_id'])
            self.start.timesheets = tts_work.create_timesheets(
                self.start.creation_type, self.start.timesheets)

        for ts in self.start.timesheets:
            ts.tts_work = Transaction().context['active_id']
        return super(CreateWorkTimesheets, self).transition_edit_timesheets()

    def default_start(self, fields):
        super_record = super(CreateWorkTimesheets, self).default_start(fields)
        tts_work_model = Pool().get(Transaction().context['active_model'])
        tts_work = tts_work_model(Transaction().context['active_id'])
        record = dict(
            work=tts_work.work.id,
            hours=tts_work.hours,
            planned_hours=tts_work.planned_hours,
            time_type=tts_work.time_type,
            creation_type='save_only',
            allowed_employees=[e.id
                for e in tts_work.team_timesheet.allowed_employees],
            timesheets=[ts.id for ts in tts_work.timesheets]
        )
        super_record.update(record)
        return super_record


class EditEmployeeTimesheetsStart(EditTimesheetsStart):
    """Edit team's timesheet of a employee"""
    __name__ = (
        'timesheet.team.timesheet-company.employee.edit_timesheets.start')

    employee = fields.Many2One('company.employee', 'Employee', readonly=True)
    day_hours = fields.TimeDelta('Day hours', 'company_work_time')

    allowed_works = fields.One2Many('timesheet.work', None, 'Allowed works',
        readonly=True, required=False)

    @classmethod
    def __setup__(cls):
        super(EditEmployeeTimesheetsStart, cls).__setup__()
        cls.timesheets.domain.extend([
            ('employee', '=', Eval('employee')),
            ('work', 'in', Eval('allowed_works'))])
        cls.timesheets.depends.extend(['allowed_works', 'employee'])


class EditEmployeeTimesheets(EditTimesheets):
    """Edit team's timesheet of a employee"""
    __name__ = 'timesheet.team.timesheet-company.employee.edit_timesheets'

    start = StateView(
        'timesheet.team.timesheet-company.employee.edit_timesheets.start',
        'team_timesheet.edit_employee_timesheets_start_view_form',
        [Button('Cancel', 'end', 'tryton-cancel'),
        Button('Update', 'edit_timesheets', 'tryton-ok', default=True)])

    def default_start(self, fields):
        super_record = super(
            EditEmployeeTimesheets,
            self).default_start(fields)
        tts_employee_model = Pool().get(Transaction().context['active_model'])
        tts_employee = tts_employee_model(Transaction().context['active_id'])

        record = dict(
            employee=tts_employee.employee.id,
            day_hours=tts_employee.day_hours,
            allowed_works=[w.id for w in
                tts_employee.team_timesheet.allowed_works],
            timesheets=[ts.id for ts in tts_employee.timesheets])

        super_record.update(record)
        return super_record
