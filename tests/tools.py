# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from proteus import Model, Wizard
from trytond.modules.company_employee_team.tests.tools import (
    create_team, get_team)
import datetime

__all__ = ['create_team_timesheet']


def create_team_timesheet(team=None, final_state='do', date=None, config=None):
    # Create team
    if not team:
        team = get_team()
    if not team:
        create_team()
        team = get_team()

    # Create team timesheet
    TeamTimesheet = Model.get('timesheet.team.timesheet')
    tts = TeamTimesheet(date=date or datetime.date.today(), team=team)
    tts.save()
    Work = Model.get('timesheet.work')
    work1 = Work(name='Work1')
    work1.save()
    tts_work = tts.works.new()
    tts_work.work = work1
    tts_work.time_type = 'employee'
    tts_work.hours = datetime.timedelta(hours=8)
    tts.save()

    if final_state != 'draft':

        # Create timesheet lines
        work, = [w for w in tts.works]
        create_work_timesheets = Wizard(
            'timesheet.team.timesheet-timesheet.work.create_timesheets',
            [work])
        create_work_timesheets.form.creation_type = 'all_employees'
        create_work_timesheets.execute('edit_timesheets')

        # Execute work flow
        for _state in ('wait', 'confirm', 'do'):
            tts.click(_state)

    return tts
