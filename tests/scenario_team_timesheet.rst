=======================
Team Timesheet Scenario
=======================

Imports::

    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, get_company
    >>> from trytond.modules.company_employee_team.tests.tools import create_team, get_team
    >>> from proteus import Model, Wizard
    >>> from datetime import timedelta, datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> today = datetime.today()

Install team_timesheet Module::

    >>> config = activate_modules('team_timesheet')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Reload the context::

    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> config._context = User.get_preferences(True, config.context)

Get today date::

    >>> import datetime
    >>> today = datetime.date.today()

Create team::

    >>> team = create_team()

Create works::

    >>> Work = Model.get('timesheet.work')
    >>> work1 = Work(name='Work1')
    >>> work1.save()

Create a team timesheet::

    >>> TeamTimesheet = Model.get('timesheet.team.timesheet')
    >>> tts = TeamTimesheet(date=today)
    >>> tts.num_workers = 5
    >>> tts.team = team
    >>> tts.num_workers == None
    True
    >>> tts.save()
    >>> tts.code == '1'
    True
    >>> tts.employees[0].employee.party.name == 'Pepito Perez'
    True
    >>> tts.state == 'draft'
    True

Add a work to a team timesheet::

    >>> tts_work = tts.works.new()
    >>> tts_work.work=work1
    >>> tts_work.time_type = 'employee'
    >>> tts_work.hours = timedelta(hours=8)
    >>> tts.save()
    >>> tts_work.planned_hours.total_seconds() / 3600
    8.0

Create work timesheets all_employees::

    >>> work, = [w for w in tts.works]
    >>> create_work_timesheets = Wizard('timesheet.team.timesheet-timesheet.work.create_timesheets', [work])
    >>> create_work_timesheets.form.creation_type = 'all_employees'
    >>> create_work_timesheets.execute('edit_timesheets')
    >>> tts = TeamTimesheet.find()
    >>> len(tts[0].works[0].timesheets)
    1
    >>> tts[0].works[0].total_hours.total_seconds() / 3600
    8.0

Modify one employee timesheet::

    >>> tts = tts[0]
    >>> employee, = [e for e in tts.employees]
    >>> edit_employee_timesheets = Wizard('timesheet.team.timesheet-company.employee.edit_timesheets', [employee])
    >>> edit_employee_timesheets.form.timesheets[0].duration = timedelta(hours=4)
    >>> edit_employee_timesheets.execute('edit_timesheets')
    >>> tts = TeamTimesheet.find()
    >>> tts[0].works[0].total_hours.total_seconds() / 3600
    4.0

Check when employee is deleted so too do its timesheets::

    >>> tts = tts[0]
    >>> len(tts.timesheets)
    1
    >>> tts.employees[0].delete()
    >>> tts.reload()
    >>> len(tts.timesheets)
    0

Workflow progress::

    >>> tts = TeamTimesheet(date=today, team=team)
    >>> tts_work = tts.works.new()
    >>> tts_work.work = work1
    >>> tts_work.time_type = 'team'
    >>> tts_work.hours = timedelta(hours=8)
    >>> tts.save()
    >>> tts.click('wait')
    >>> len(tts.timesheets)
    1
    >>> tts.click('confirm')
    >>> tts.click('do')
    >>> tts.reload()
    >>> tts.works[0].total_hours.total_seconds() / 3600
    8.0
    >>> len(tts.timesheets)
    1

Check line can not be modified when state is confirmed or done::

    >>> tts.state
    'done'
    >>> tts.timesheets[0].duration = timedelta(hours=10)
    >>> tts.timesheets[0].save()
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: You cannot modify line "2" because its Team Timesheet "2" is in "Confirmed" or "Done" state. - 

Check line can not be deleted when its team timesheet is done::

    >>> tts.timesheets[0].delete()
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: You cannot delete line "2" because its Team Timesheet "2" is in "Done" state. - 

Create new employee to change the timesheet::

    >>> Party = Model.get('party.party')
    >>> new_party = Party(name='New Pepito')
    >>> new_party.save()
    >>> Employee = Model.get('company.employee')
    >>> new_employee = Employee(company=company, party=new_party)
    >>> new_employee.save()

Check timesheet's employees must be included in team timesheet's employees::

    >>> tts.click('draft')
    >>> tts.click('wait')
    >>> tts.timesheets[0].employee = new_employee
    >>> tts.timesheets[0].save()
    >>> tts.click('confirm')
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Timesheets employees must be included in Team Timesheet "2". - 
    >>> tts.timesheets[0].employee = tts.employees[0].employee
    >>> tts.timesheets[0].save()
    >>> tts.click('confirm')
    >>> tts.click('do')

Check duplicated timesheets::

    >>> duplicated, = tts.duplicate()
    >>> duplicated.click('wait')
    >>> duplicated.click('confirm')
    >>> duplicated.click('do') # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
    	...
    trytond.exceptions.UserWarning: Team "A Team" has already a team timesheet on "...". - 

Check team work domain on Timesheet work::

    >>> work2 = Work(name='Work 2')
    >>> work2.save()
    >>> work1 = Work(work1.id)
    >>> team.works.append(work2)
    >>> team.save()
    >>> team = get_team()
    >>> tts = TeamTimesheet(date=today, team=team)
    >>> tts_work = tts.works.new()
    >>> tts_work.work == work2
    True
    >>> tts_work.work = work1
    >>> tts_work.time_type = 'team'
    >>> tts_work.hours = timedelta(hours=8)
    >>> tts.save()
    Traceback (most recent call last):
    	...
    trytond.model.modelstorage.DomainValidationError: The value for field "Work" in "TeamTimesheet - Work" is not valid according to its domain. - 
    >>> _ = team.works.pop()
    >>> team.save()
    >>> team = get_team()
    >>> tts.save()

Check duplicate works::

    >>> tts = TeamTimesheet(date=today, team=team)
    >>> tts_work = tts.works.new()
    >>> tts_work.work = work1
    >>> tts_work.time_type = 'employee'
    >>> tts_work.hours = timedelta(hours=8)
    >>> tts.save()
    >>> tts.click('wait')
    >>> len(tts.timesheets)
    1
    >>> tts.click('draft')
    >>> len(tts.timesheets)
    1
    >>> tts_work = tts.works.new()
    >>> tts_work.work = work1
    >>> tts_work.time_type = 'employee'
    >>> tts_work.hours = timedelta(hours=8)
    >>> tts.save() # doctest: +IGNORE_EXCEPTION_DETAIL
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: "Work1" work of teamtimesheet "..." must be unique. - 
    >>> tts_work.work = work2
    >>> tts.save()
    >>> tts.click('wait')
    >>> len(tts.timesheets)
    2

Create team timesheet without team::

    >>> tts = TeamTimesheet(date=today, num_workers=7)
    >>> tts_work = tts.works.new()
    >>> tts_work.work = work1
    >>> tts_work.hours = timedelta(hours=3)
    >>> tts_work.planned_hours.total_seconds() / 3600
    21.0
    >>> tts_work = tts.works.new()
    >>> tts_work.work = work2
    >>> tts_work.hours = timedelta(hours=7)
    >>> tts_work.planned_hours.total_seconds() / 3600
    49.0
    >>> tts.save()
    >>> tts.click('wait')
    >>> tts.click('confirm')
    >>> tts.click('do')

Check delete timesheets::

    >>> Tts_line = Model.get('timesheet.line')
    >>> tts = TeamTimesheet(date=today, team=team)
    >>> bool(tts.employees)
    True
    >>> tts.save()
    >>> tts_work = tts.works.new()
    >>> tts_work.work = work1
    >>> tts_work.time_type = 'employee'
    >>> tts_work.hours = timedelta(hours=8)
    >>> tts.save()
    >>> tts.click('wait')
    >>> tts_lines = tts.works[0].timesheets
    >>> len(tts_lines)
    1
    >>> Model.get('res.user.warning')(user=config.user,
    ...     name='0.cannot_delete', always=True).save()
    >>> tts.works[0].delete()
    >>> tts.reload()
    >>> not tts.works
    True
    >>> len(Tts_line.find([('id', 'in', list(map(int, tts_lines)))]))
    0

Check change date in lines::

    >>> tts, = TeamTimesheet.find([('code', '=', '2')], limit=1)
    >>> len(tts.timesheets)
    1
    >>> tts.click('draft')
    >>> tts.date == tts.timesheets[0].date == today
    True
    >>> tts.date = today + relativedelta(days=1)
    >>> tts.save()
    >>> tts.date == tts.timesheets[0].date == today + relativedelta(days=1)
    True

Workflow progress with time = 0h::

    >>> tts2 = TeamTimesheet(date=today, team=team)
    >>> tts_work2 = tts2.works.new()
    >>> tts_work2.work = work1
    >>> tts_work2.time_type = 'team'
    >>> tts_work2.hours = timedelta(hours=0)
    >>> tts2.save()
    >>> tts2.click('wait')
    >>> len(tts2.timesheets)
    1
    >>> tts2.click('confirm')
    >>> tts2.click('do')
    >>> len(tts2.timesheets)
    0

Check Delete timesheet lines with 0h at confirmation::

    >>> tts3 = TeamTimesheet(date=today, team=team)
    >>> tts3.date = today + relativedelta(days=1)
    >>> tts_work3 = tts3.works.new()
    >>> tts_work3.work = work1
    >>> tts_work3.time_type = 'team'
    >>> tts_work3.hours = timedelta(hours=8)
    >>> tts3.save()
    >>> tts3.click('wait')
    >>> len(tts3.timesheets)
    1
    >>> timesheet_line = tts3.timesheets[0]
    >>> timesheet_line.duration = timedelta(hours=0)
    >>> timesheet_line.save()
    >>> tts3.click('confirm')
    >>> tts3.click('do')
    >>> len(tts3.timesheets)
    0